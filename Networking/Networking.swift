//
//  Networking.swift
//  Networking
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation

func performRequest(for url: URL, completionHandler: @escaping ((Result<Data, Error>) -> Void)) {
    let urlSession = URLSession.shared
    let task = urlSession.dataTask(with: url) { (data, response, error) in
        guard error == nil else {
            completionHandler(.failure(NetworkingError.internalError(error!)))
            return
        }
        guard let httpResponse = response as? HTTPURLResponse, let data = data else {
            completionHandler(.failure(NetworkingError.badResponse))
            return
        }
        guard isStatusCode200(forResponse: httpResponse) else {
            do {
                try handleNon200(response: httpResponse)
            } catch {
                completionHandler(.failure(error))
            }
            return
        }
        completionHandler(.success(data))
    }
    task.resume()
}

private func isStatusCode200(forResponse response: HTTPURLResponse) -> Bool {
    return response.statusCode == 200
}

private func handleNon200(response: HTTPURLResponse) throws {
    if response.statusCode == 404 {
        throw NetworkingError.notFound
    } else if response.statusCode == 400 {
        throw NetworkingError.badRequest
    } else {
        throw NetworkingError.badResponse
    }
}

public enum NetworkingError: Error {
    case internalError(Error)
    case notFound
    case badRequest
    case badResponse
}
