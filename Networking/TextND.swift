//
//  TextND.swift
//  Networking
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
struct FetchedTextND: Codable {
    let batchcomplete: String
    let query: QueryND
    
    func toViewData() -> TextVD {
        return TextVD(text: query.pages.first!.value.extract)
    }
}

struct QueryND: Codable {
    let pages: [String: TextND]
}

struct TextND: Codable {
    let pageid: Int
    let ns: Int
    let title: String
    let extract: String
}

enum DataConversionError: Error {
    case wikiTextVDConversionFailed
}
