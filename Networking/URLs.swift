//
//  URLs.swift
//  Networking
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation

enum URLs {
    static func getTextFetchUrl(for object: String) -> URL {
        var urlComponents = URLComponents()
        urlComponents.scheme = wikiTextFetchScheme
        urlComponents.host = wikiTextFetchHost
        urlComponents.path = wikiTextFetchPath
        urlComponents.queryItems = [
            URLQueryItem(name: QueryKeyConstants.action.rawValue, value: QueryValueConstants.query.rawValue),
            URLQueryItem(name: QueryKeyConstants.format.rawValue, value: QueryValueConstants.json.rawValue),
            URLQueryItem(name: QueryKeyConstants.titles.rawValue, value: object),
            URLQueryItem(name: QueryKeyConstants.prop.rawValue, value: QueryValueConstants.extracts.rawValue),
            URLQueryItem(name: QueryKeyConstants.explaintext.rawValue, value: QueryValueConstants.explaintext.rawValue),
            URLQueryItem(name: QueryKeyConstants.exlimit.rawValue, value: QueryValueConstants.exlimit.rawValue)
        ]
        return urlComponents.url!
    }
}

private enum QueryKeyConstants: String {
    case action
    case format
    case titles
    case prop
    case explaintext
    case exlimit
}

private enum QueryValueConstants: String {
    case query
    case json
    case extracts
    case explaintext = "true"
    case exlimit = "1"
}

private let wikiTextFetchScheme = "https"
private let wikiTextFetchHost = "en.wikipedia.org"
private let wikiTextFetchPath = "/w/api.php"
