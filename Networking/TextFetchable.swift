//
//  TextFetchable.swift
//  Networking
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation

public protocol TextFetchable: class {}

extension TextFetchable {
    public func fetchTextFromWiki(forObject object: String, completionHandler: @escaping((Result<TextVD, Error>) -> Void)) {
        let url = URLs.getTextFetchUrl(for: object)
        performRequest(for: url) { [weak self] response in
            switch response {
            case .success(let data):
                do {
                    guard let this = self else {
                        completionHandler(.failure(DecodingError.deinitialisedSelf))
                        return
                    }
                    let wikiText = try this.getVDFrom(response: data)
                    completionHandler(.success(wikiText))
                } catch {
                    completionHandler(.failure(error))
                }
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
    private func getVDFrom(response: Data) throws -> TextVD {
        do {
            let jsonDecoder = JSONDecoder()
            let data = try jsonDecoder.decode(FetchedTextND.self, from: response)
            return data.toViewData()
        } catch {
            throw DecodingError.wikiTextNDDecodeFailed
        }
    }
}

enum DecodingError: Error {
    case wikiTextNDDecodeFailed
    case deinitialisedSelf
}
