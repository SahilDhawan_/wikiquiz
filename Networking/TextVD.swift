//
//  TextVD.swift
//  Networking
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation

public struct TextVD {
    public let text: String
    
    init(text: String) {
        self.text = text
    }
}
