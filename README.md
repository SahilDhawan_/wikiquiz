# WikiQuiz
A user is displayed content from a random Wikipedia page. 10 words are removed from the content to create blanks. The user needs to fill in these blanks using a jumbled list of words that were removed. He/she gets a score out of 10, depending on the number of blanks that were correctly filled.

### Key Code Decisions
1. Support for iOS 9 and above.
2. Support for Auto Layout written in code as compared to storyboards or xib to improve build time.
3. Usage of static libraries for better code management.
4. Universal Application ( can be used on both iPhone and iPad in both the orientations ).
5. Support for theme updation in future.

### Application Architecture
There is no well defined architectural model which is followed in this app but it closely resembles MVC. 
