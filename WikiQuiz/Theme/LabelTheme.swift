//
//  LabelTheme.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

struct LabelTheme {
    let fontSize: CGFloat
    let fontColor: UIColor
    let textAlignment: NSTextAlignment
    let numberOfLines: Int
    let backgroundColor: UIColor?
    
    init(
        fontSize: CGFloat,
        fontColor: UIColor,
        textAlignment: NSTextAlignment,
        numberOfLines: Int,
        backgroundColor: UIColor? = nil
    ) {
        self.fontSize = fontSize
        self.fontColor = fontColor
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
        self.backgroundColor = backgroundColor
    }
}

extension UILabel {
    func apply(theme: LabelTheme) {
        textAlignment = theme.textAlignment
        numberOfLines = theme.numberOfLines
        font = UIFont.systemFont(ofSize: theme.fontSize)
        textColor = theme.fontColor
        if let color = theme.backgroundColor {
            backgroundColor = color
        }
    }
}
