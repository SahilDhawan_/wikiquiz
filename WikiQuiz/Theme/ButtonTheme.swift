//
//  ButtonTheme.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

struct ButtonTheme {
     let fontSize: CGFloat
     let fontColor: UIColor
     let backgroundColor: UIColor
    
    init(fontSize: CGFloat, fontColor: UIColor, backgroundColor: UIColor) {
         self.fontColor = fontColor
         self.fontSize = fontSize
         self.backgroundColor = backgroundColor
     }
}

extension UIButton {
    func apply(theme: ButtonTheme) {
        setTitleColor(theme.fontColor, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: theme.fontSize)
        backgroundColor = theme.backgroundColor
    }
}
