//
//  Theme.swift
//  WikiQuiz
//
//  Created by R K Dhawan on 03/10/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

protocol Theme {
    var preBlankWordLabel: LabelTheme { get }
    var postBlankWordLabel: LabelTheme { get }
    var blankWordButton: ButtonTheme { get }
    var blankWordsCollectionViewCellColor: UIColor { get }
    var blankWordsCollectionViewCellTitleLabel: LabelTheme { get }
    var blankWordsCollectionViewBackgroundColor: UIColor { get }
    var quizViewBackgroundColor: UIColor { get }
    var quizLineRepresentationFont: UIFont { get }
    var quizSubmitButton: ButtonTheme { get }
    var blankWordBorderColor: UIColor { get }
    var scoreLabel: LabelTheme { get }
    var scoreViewBackgroundColor: UIColor { get }
    var scoreCorrectWordLabel: LabelTheme { get }
    var scoreAnsweredWordLabel: LabelTheme { get }
    var correctAnsweredWordColor: UIColor { get }
    var scoreViewReplayButton: ButtonTheme { get }
}

struct DarkTheme: Theme {
    let preBlankWordLabel: LabelTheme = LabelTheme(
        fontSize: 17,
        fontColor: UIColor.white,
        textAlignment: .left,
        numberOfLines: 1
    )
    let postBlankWordLabel: LabelTheme = LabelTheme(
        fontSize: 17,
        fontColor: UIColor.white,
        textAlignment: .left,
        numberOfLines: 1
    )
    let blankWordButton: ButtonTheme = ButtonTheme(
        fontSize: 17,
        fontColor: UIColor.black,
        backgroundColor: UIColor.yellow.withAlphaComponent(0.5)
    )
    let blankWordsCollectionViewCellColor: UIColor = UIColor.yellow.withAlphaComponent(0.5)
    let blankWordsCollectionViewCellTitleLabel: LabelTheme = LabelTheme(
           fontSize: 17,
           fontColor: UIColor.black,
           textAlignment: .left,
           numberOfLines: 1
    )
    let blankWordsCollectionViewBackgroundColor: UIColor = UIColor.black
    let quizViewBackgroundColor: UIColor = UIColor.black
    let quizLineRepresentationFont: UIFont = UIFont.systemFont(ofSize: 17)
    let quizSubmitButton: ButtonTheme = ButtonTheme(
        fontSize: 18,
        fontColor: UIColor.black,
        backgroundColor: UIColor.yellow.withAlphaComponent(0.5)
    )
    let blankWordBorderColor: UIColor = UIColor.black
    let scoreLabel: LabelTheme = LabelTheme(
           fontSize: 20,
           fontColor: .white,
           textAlignment: .center,
           numberOfLines: 1
   )
    let scoreViewBackgroundColor: UIColor = UIColor.black
    let scoreCorrectWordLabel: LabelTheme = LabelTheme(
        fontSize: 17,
        fontColor: .white,
        textAlignment: .center,
        numberOfLines: 1,
        backgroundColor: greenColor
    )
    let scoreAnsweredWordLabel: LabelTheme = LabelTheme(
        fontSize: 17,
        fontColor: .white,
        textAlignment: .center,
        numberOfLines: 1,
        backgroundColor: redColor
    )
    let correctAnsweredWordColor: UIColor = greenColor
    let scoreViewReplayButton: ButtonTheme = ButtonTheme(
        fontSize: 18,
        fontColor: .black,
        backgroundColor: UIColor.yellow.withAlphaComponent(0.5)
    )
}

struct CurrentTheme {
    static var shared: Theme = DarkTheme()
}

private let greenColor = UIColor(red: 0.101, green: 0.290, blue: 0.023, alpha: 1.0)
private let redColor = UIColor(red: 0.556, green: 0.027, blue: 0.027, alpha: 1.0)
