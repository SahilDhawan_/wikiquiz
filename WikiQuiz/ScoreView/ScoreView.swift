//
//  ScoreView.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 01/10/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

class ScoreView: UIView {
    private weak var scoreLabel: UILabel!
    private weak var scoreTableView: UITableView!
    private weak var replayButton: UIButton!
    private weak var delegate: ScoreViewDelegate!
    
    private var userScore: Int = 0
    private let correctWords: [Int: String]
    private let answeredWords: [Int: String]
    private let tableViewCellIdentifier = "ScoreViewTableViewCellIdentifier"
    
    init(
        withCorrectWords correctWords: [Int: String],
        answeredWords: [Int: String],
        andDelegate delegate: ScoreViewDelegate
    ) {
        self.correctWords = correctWords
        self.answeredWords = answeredWords
        self.delegate = delegate
        super.init(frame: .zero)
        calculateUserScore()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // calculates user score by comparing the correct word with the word answered by the user. The score is updated by 1 if the word answered by user matches with the correct value
    private func calculateUserScore() {
        for key in correctWords.keys {
            let correctValue = self.correctWords[key]
            let answeredValue = self.answeredWords[key]
            if answeredValue == correctValue {
                userScore += 1
            }
        }
    }
}

// MARK: -

// MARK : UI Setup

extension ScoreView {
    private func setupView() {
        var constraints: [NSLayoutConstraint] = []
        constraints += setupScoreLabel()
        constraints += setupScoreTableView()
        constraints += setupReplayButton()
        NSLayoutConstraint.activate(constraints)
        backgroundColor = CurrentTheme.shared.scoreViewBackgroundColor
    }
    
    private func setupScoreLabel() -> [NSLayoutConstraint] {
        let scoreLabel = UILabel()
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        scoreLabel.text = "Your Score is \(String(describing: userScore)) / \(correctWords.count)"
        scoreLabel.apply(theme: CurrentTheme.shared.scoreLabel)
        self.scoreLabel = scoreLabel
        addSubview(self.scoreLabel)
        
        return [
            self.scoreLabel.topAnchor.constraint(equalTo: topAnchor, constant: scoreLabelTopConstant),
            self.scoreLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: scoreLabelLeadingConstant),
            self.scoreLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: scoreLabelTrailingConstant),
            self.scoreLabel.heightAnchor.constraint(equalToConstant: scoreLabelHeight)
        ]
    }
    
    private func setupScoreTableView() -> [NSLayoutConstraint] {
        let scoreTableView = UITableView()
        scoreTableView.dataSource = self
        scoreTableView.delegate = self
        scoreTableView.register(ScoreTableViewCell.self, forCellReuseIdentifier: tableViewCellIdentifier)
        scoreTableView.backgroundColor = CurrentTheme.shared.scoreViewBackgroundColor
        scoreTableView.translatesAutoresizingMaskIntoConstraints = false
        scoreTableView.tableFooterView = UIView()
        self.scoreTableView = scoreTableView
        addSubview(self.scoreTableView)
        
        return [
            scoreTableView.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor, constant: scoreTableViewCellTopConstant),
            scoreTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: scoreTableViewCellLeadingConstant),
            scoreTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: scoreTableViewCellTrailingConstant)
        ]
    }
    
    private func setupReplayButton() -> [NSLayoutConstraint] {
        let replayButton = UIButton()
        replayButton.translatesAutoresizingMaskIntoConstraints = false
        replayButton.setTitle("Let me play again", for: .normal)
        replayButton.titleEdgeInsets = replayButtonEdgeInsets
        replayButton.apply(theme: CurrentTheme.shared.scoreViewReplayButton)
        replayButton.addTarget(self, action: #selector(onReplayButtonTap), for: .touchUpInside)
        self.replayButton = replayButton
        addSubview(self.replayButton)
        
        return [
            self.replayButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            self.replayButton.topAnchor.constraint(equalTo: scoreTableView.bottomAnchor, constant: replayButtonTopConstant),
            self.replayButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: replayButtonBottomConstant),
            self.replayButton.heightAnchor.constraint(equalToConstant: replayButtonHeightConstant),
            self.replayButton.widthAnchor.constraint(equalToConstant: 200)
        ]
    }
    
    @objc private func onReplayButtonTap() {
        delegate.onReplayButtonTap()
    }
}

// MARK: -

// MARK: - UITableViewDataSource

extension ScoreView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return correctWords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath) as! ScoreTableViewCell
        let answeredWord: String = answeredWords[indexPath.item] ?? ""
        let correctWord: String = correctWords[indexPath.item] ?? ""
        cell.setLabelTitles(
            withCorrectWord: correctWord,
            andAnsweredWord: answeredWord
        )
        cell.selectionStyle = .none
        cell.backgroundColor = CurrentTheme.shared.scoreViewBackgroundColor
        return cell
    }
}

// MARK: -

// MARK: - UITableViewDelegate

extension ScoreView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewCellHeight
    }
}

protocol ScoreViewDelegate: class {
    func onReplayButtonTap()
}

// MARK: -

// MARK: - Constants

private let replayButtonTopConstant: CGFloat = 10
private let replayButtonBottomConstant: CGFloat = -10
private let replayButtonHeightConstant: CGFloat = 40
private let replayButtonEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

private let scoreLabelTopConstant: CGFloat = 10
private let scoreLabelLeadingConstant: CGFloat = 10
private let scoreLabelTrailingConstant: CGFloat = -10
private let scoreLabelHeight: CGFloat = 40

private let tableViewCellHeight: CGFloat = 45
private let scoreTableViewCellLeadingConstant: CGFloat = 10
private let scoreTableViewCellTrailingConstant: CGFloat = -10
private let scoreTableViewCellTopConstant: CGFloat = 10
private let scoreTabelViewCellBottomConstant: CGFloat = -10
