//
//  ScoreTableViewCell.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 02/10/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

class ScoreTableViewCell: UITableViewCell {
    private weak var correctWordLabel: UILabel!
    private weak var answeredWordLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    private func setupView() {
        var constraints: [NSLayoutConstraint] = []
        constraints += setupCorrectWordLabel()
        constraints += setupAnswredWordLabel()
        NSLayoutConstraint.activate(constraints)
    }
    
    func setLabelTitles(withCorrectWord correctWord: String, andAnsweredWord answeredWord: String) {
        correctWordLabel.text = correctWord
        answeredWordLabel.text = answeredWord
        if correctWord == answeredWord {
            answeredWordLabel.backgroundColor = CurrentTheme.shared.correctAnsweredWordColor
        }
    }
    
    private func setupCorrectWordLabel() -> [NSLayoutConstraint] {
        let correctWordLabel = UILabel()
        correctWordLabel.translatesAutoresizingMaskIntoConstraints = false
        correctWordLabel.apply(theme: CurrentTheme.shared.scoreCorrectWordLabel)
        self.correctWordLabel = correctWordLabel
        addSubview(self.correctWordLabel)
        
        return [
            self.correctWordLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            self.correctWordLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            self.correctWordLabel.trailingAnchor.constraint(equalTo: centerXAnchor, constant: -10),
            self.correctWordLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10)
        ]
    }
    
    private func setupAnswredWordLabel() -> [NSLayoutConstraint] {
        let answeredWordLabel = UILabel()
        answeredWordLabel.translatesAutoresizingMaskIntoConstraints = false
        answeredWordLabel.apply(theme: CurrentTheme.shared.scoreAnsweredWordLabel)
        self.answeredWordLabel = answeredWordLabel
        addSubview(self.answeredWordLabel)
        
        return [
            self.answeredWordLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            self.answeredWordLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            self.answeredWordLabel.leadingAnchor.constraint(equalTo: centerXAnchor, constant: 10),
            self.answeredWordLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        ]
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
