//
//  QuizCoordinator.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import Networking
import UIKit

class QuizCoordinator {
    private var lineStartIndex: Int = 0
    private var currentTopic: String!
    
    // returns the viewControlller to be set as the rootViewController
    func presentRootViewController() -> UIViewController {
        return QuizViewController(delegate: self)
    }
}

extension QuizCoordinator: QuizCoordinatorDelegate {
    /// Fetches text from the data source in a form which can be used for the quiz
    /// - Parameter font: Font size used by the labels or any UI element which will represent the quiz lines
    /// - Parameter width: Width of the label or any UI element which will represent the quiz lines
    /// - Parameter numberOfLines: The number of lines of text required for the quiz
    /// - Parameter completionHandler: The result of fetching text for the quiz
    func fetchQuizLines(
        forFont font: UIFont,
        width: CGFloat,
        numberOfLines: Int,
        completionHandler: @escaping ((Result<[String], Error>
    ) -> Void)){
        lineStartIndex = 0
        currentTopic = getRandomTopicForQuiz()
        print("Fetching text for topic \(String(describing: currentTopic))")
        fetchTextFromWiki(forObject: currentTopic) { [weak self] response in
            switch response {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let wikiText):
                let quizLines = self?.getQuizLines(
                    fromString: wikiText.text,
                    font: font,
                    andWidth: width,
                    numberOfLines: numberOfLines
                )
                completionHandler(.success(quizLines!))
            }
        }
    }
    
       /// Fetches text from the data source in a form which can be used for the quiz
       /// - Parameter topic: Topic which should be discarded for the quix text
       /// - Parameter font: Font size used by the labels or any UI element which will represent the quiz lines
       /// - Parameter width: Width of the label or any UI element which will represent the quiz lines
       /// - Parameter numberOfLines: The number of lines of text required for the quiz
       /// - Parameter completionHandler: The result of fetching text for the quiz
    private func fetchQuizLines(
           forTopicApartFrom topic: String,
           forFont font: UIFont,
           width: CGFloat,
           numberOfLines: Int,
           completionHandler: @escaping ((Result<[String], Error>
    ) -> Void)) {
        lineStartIndex = 0
        currentTopic = getNewTopicApartFrom(topic)
        print("Fetching text for topic \(String(describing: currentTopic))")
        fetchTextFromWiki(forObject: currentTopic) { [weak self] response in
            switch response {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let wikiText):
                let quizLines = self?.getQuizLines(
                    fromString: wikiText.text,
                    font: font,
                    andWidth: width,
                    numberOfLines: numberOfLines
                )
                completionHandler(.success(quizLines!))
            }
        }
    }
    
    /// Converts fetched text into multiple lines which are required for quiz
    /// - Parameter string: The multiline fetched text which needs to be converted to [String] to be represented as multiple lines for quiz
    /// - Parameter font: The font of the label or any UI element which is used to represent the quiz line
    /// - Parameter width: Width of the label or any UI element which will represent the quiz lines
    /// - Parameter numberOfLines: The number of lines of text required for the quiz
    private func getQuizLines(fromString string: String, font: UIFont, andWidth width: CGFloat, numberOfLines: Int) -> [String] {
        let textCharacters = string.split(separator: " ")
        var quizLines: [String] = []
        for _ in 0 ..< numberOfLines {
            var firstLineFromWikiText: String = ""
            for index in lineStartIndex ..< textCharacters.count {
                let stringAttributes = [NSAttributedString.Key.font: font]
                var tempLineText = firstLineFromWikiText
                tempLineText += textCharacters[index] + " "
                let firstLineTextWidth = (tempLineText as NSString).size(withAttributes: stringAttributes).width
                if firstLineTextWidth < width {
                    firstLineFromWikiText = tempLineText
                } else {
                    lineStartIndex = index
                    quizLines.append(firstLineFromWikiText)
                    break
                }
            }
        }
        return quizLines
    }
    
    func replayQuiz (
        forFont font: UIFont,
        width: CGFloat,
        numberOfLines: Int,
        completionHandler: @escaping ((Result<[String], Error>) -> Void )
    ){
        fetchQuizLines(
            forTopicApartFrom: currentTopic,
            forFont: font,
            width: width,
            numberOfLines: numberOfLines,
            completionHandler: { response in
                switch response {
                case .success(let quizLines):
                    completionHandler(.success(quizLines))
                case .failure(let error):
                    completionHandler(.failure(error))
                }
        })
    }
    
    private func getRandomTopicForQuiz() -> String {
        let randomTopicIndex = Int.random(in: Range(uncheckedBounds: (lower: 0, upper: topics.count)))
        let randomTopic = topics[randomTopicIndex]
        return randomTopic
    }
    
    private func getNewTopicApartFrom(_ topic: String) -> String {
        let filteredTopics = topics.filter({ $0 != topic })
        let randomTopicIndex = Int.random(in: Range(uncheckedBounds: (lower: 0, upper: filteredTopics.count)))
        let randomTopic = filteredTopics[randomTopicIndex]
        return randomTopic
    }
}

protocol QuizCoordinatorDelegate: class {
    func fetchQuizLines(
        forFont font: UIFont,
        width: CGFloat,
        numberOfLines: Int,
        completionHandler: @escaping ((Result<[String], Error>
    ) -> Void))
    
    func replayQuiz (
        forFont font: UIFont,
        width: CGFloat,
        numberOfLines: Int,
        completionHandler: @escaping ((Result<[String], Error>) -> Void )
    )
}

extension QuizCoordinator: TextFetchable {}

// MARK: -

// MARK: Constants

private let topics = ["Apple", "Banana", "Cherry", "Kiwi", "Lemon"]
