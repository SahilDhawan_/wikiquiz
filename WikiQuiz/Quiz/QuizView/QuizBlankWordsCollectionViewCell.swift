//
//  BlankWordsCollectionViewCell.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 29/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

class BlankWordsCollectionViewCell: UICollectionViewCell {
    private weak var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var constraints: [NSLayoutConstraint] = []
        constraints += setupTitleLabelText()
        NSLayoutConstraint.activate(constraints)
        backgroundColor = CurrentTheme.shared.blankWordsCollectionViewCellColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: -

// MARK: UI Setup

extension BlankWordsCollectionViewCell {
    private func setupTitleLabelText() -> [NSLayoutConstraint] {
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.apply(theme: CurrentTheme.shared.blankWordsCollectionViewCellTitleLabel)
        self.titleLabel = titleLabel
        addSubview(self.titleLabel)
        
        return [
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: titleLabelLeadingConstant),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: titleLabelTrailingConstant),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: titleLabelBottomConstant),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: titleLabelTopConstant)
        ]
    }
    
    func setTitleLabelText(to text: String) {
        titleLabel.text = text
    }
}

// MARK: -

// MARK: Constants

private let titleLabelTopConstant: CGFloat = 10
private let titleLabelLeadingConstant: CGFloat = 10
private let titleLabelTrailingConstant: CGFloat = -10
private let titleLabelBottomConstant: CGFloat = -10
