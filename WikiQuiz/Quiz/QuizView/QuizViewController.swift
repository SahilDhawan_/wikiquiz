//
//  QuizViewController.swift
//  WikipediaGame
//
//  Created by Sahil Dhawan on 28/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Networking
import UIKit

class QuizViewController: UIViewController {
    private weak var quizTextContainerView: UIView!
    private weak var blankWordsCollectionView: UICollectionView!
    private weak var blankWordsCollectionViewFlowLayout: UICollectionViewFlowLayout!
    private weak var containerScrollView: UIScrollView!
    private var quizTextRepresentationViews: [QuizLineRepresentationView] = []
    private weak var submitButton: UIButton!
    private weak var scoreView: ScoreView!
    private weak var themeChangeButton: UIButton!
    private weak var activityIndicatorView: UIActivityIndicatorView!
    
    private var quizLines: [String] = []
    private var quizBlankWords: [String] = []
    private var correctBlankWordsOrder: [Int: String] = [:]
    private var answeredBlankWordsOrder: [Int: String] = [:]
    private let blankWordsCollectionViewIdentifier = "blankWordsCollectionViewIdentifier"
    private var lastBlankWordIndexTapped: Int!
    
    private var submitButtonPortraitConstraints: [NSLayoutConstraint] = []
    private var submitButtonLandscapeConstraints: [NSLayoutConstraint] = []
    
    private var blankWordsCollectionViewPortraitConstraints: [NSLayoutConstraint] = []
    private var blankWordsCollectionViewLandsapeConstraints: [NSLayoutConstraint] = []
    
    private weak var delegate: QuizCoordinatorDelegate!
    
    init(delegate: QuizCoordinatorDelegate) {
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func startQuiz() {
        delegate.fetchQuizLines(
            forFont: CurrentTheme.shared.quizLineRepresentationFont,
            width: getWidthForQuizText(),
            numberOfLines: numberOfRowsRequiredForQuiz
        ) { [weak self] response in
            DispatchQueue.main.async {
               self?.activityIndicatorView.stopAnimating()
            }
            switch response {
            case .success(let quizLines):
                self?.quizLines = quizLines
                self?.setupView()
            case .failure(let error):
                print(error)
                // handle error
            }
        }
    }
}

// MARK: -

// MARK: View life cycle methods

extension QuizViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = CurrentTheme.shared.quizViewBackgroundColor
        startQuiz()
        setupActivityIndicatorView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if submitButton != nil {
            updateBlankWordsCollectionViewConstraints()
            updateSubmitButtonConstraints()
            view.layoutIfNeeded()
        }
        if isOrientationPortrait() {
            if blankWordsCollectionView?.isHidden == false {
                submitButton?.isHidden = true
            }
        } else {
            if blankWordsCollectionView?.isHidden == false {
                submitButton?.isHidden = false
            }
        }
    }
}

// MARK: -

// MARK: UI Setup

extension QuizViewController {
    private func setupView() {
       DispatchQueue.main.async { [weak self] in
           guard let this = self else {
               print("Deinitialised self in setupView() in QuizViewController.swift")
               return
           }
           var constraints: [NSLayoutConstraint] = []
           constraints += this.setupContainerScrollView()
           constraints += this.setupQuizTextContainerView()
           constraints += this.setupQuizTextRepresentationView()
           this.quizBlankWords = this.quizBlankWords.sorted {
               return $0 < $1
           }
           NSLayoutConstraint.activate(constraints)
           this.setupBlankWordsCollectionView()
           this.setupTapGestureRecognizer()
           this.setupSubmitButton()
       }
    }
    
    private func updateBlankWordsCollectionViewConstraints() {
        if isOrientaionLandscape() {
            blankWordsCollectionViewFlowLayout.scrollDirection = .vertical
            for constraint in blankWordsCollectionViewPortraitConstraints {
                constraint.isActive = false
            }
            for constraint in blankWordsCollectionViewLandsapeConstraints {
                constraint.isActive = true
            }
        } else {
            blankWordsCollectionViewFlowLayout.scrollDirection = .horizontal
            for constraint in blankWordsCollectionViewLandsapeConstraints {
                constraint.isActive = false
            }
            for constraint in blankWordsCollectionViewPortraitConstraints {
                constraint.isActive = true
            }
        }
    }
    
    private func updateSubmitButtonConstraints() {
        if isOrientaionLandscape() {
            submitButton.titleLabel?.numberOfLines = 3
            for constraint in submitButtonPortraitConstraints {
                constraint.isActive = false
            }
            for constraint in submitButtonLandscapeConstraints {
                constraint.isActive = true
            }
        } else {
            submitButton.titleLabel?.numberOfLines = 1
            for constraint in submitButtonPortraitConstraints {
                constraint.isActive = true
            }
            for constraint in submitButtonLandscapeConstraints {
                constraint.isActive = false
            }
        }
    }
    
    private func getWidthForQuizText() -> CGFloat {
        let containerWidth: CGFloat
        if isOrientaionLandscape() {
            if UIScreen.main.bounds.height - quizLineWidthDifference >= scrollViewContainerWidthConstant {
                containerWidth = scrollViewContainerWidthConstant
            } else {
                containerWidth = UIScreen.main.bounds.height - quizLineWidthDifference
            }
        } else {
            if UIScreen.main.bounds.width - quizLineWidthDifference >= scrollViewContainerWidthConstant {
                containerWidth = scrollViewContainerWidthConstant
            } else {
                containerWidth = UIScreen.main.bounds.width - quizLineWidthDifference
            }
        }
        return containerWidth
    }
    
    private func setupTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap))
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc private func onViewTap() {
        hideBlankWordsCollectionView()
    }
     
    private func setupContainerScrollView() -> [NSLayoutConstraint] {
        let containerScrollView = UIScrollView()
        containerScrollView.translatesAutoresizingMaskIntoConstraints = false
        containerScrollView.showsHorizontalScrollIndicator = false
        containerScrollView.showsVerticalScrollIndicator = false
        self.containerScrollView = containerScrollView
        view.addSubview(self.containerScrollView)
                
        var constraints : [NSLayoutConstraint] = []
        constraints += getScrollViewWidthConstraints()
        constraints += getScrollViewHeightConstraints()
        constraints += [
            self.containerScrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            self.containerScrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ]
        return constraints
    }
    
    private func getScrollViewWidthConstraints() -> [NSLayoutConstraint] {
        let leadingConstraint = self.containerScrollView.leadingAnchor.constraint(
            lessThanOrEqualTo: view.leadingAnchor,
            constant: scrollViewContainerLeadingConstant
        )
        leadingConstraint.priority = lowPriorityConstraintValue
               
        let trailingConstraint = self.containerScrollView.trailingAnchor.constraint(
            lessThanOrEqualTo: view.trailingAnchor,
            constant: scrollViewContainerTrailingConstant
        )
        trailingConstraint.priority = lowPriorityConstraintValue
        
        let widthConstraint = self.containerScrollView.widthAnchor.constraint(greaterThanOrEqualToConstant: getScrollViewWidth())
        widthConstraint.priority = lowPriorityConstraintValue
        
        return [
            self.containerScrollView.widthAnchor.constraint(lessThanOrEqualToConstant: scrollViewContainerWidthConstant),
            leadingConstraint,
            trailingConstraint,
            widthConstraint
        ]
    }
    
    private func getScrollViewHeightConstraints() -> [NSLayoutConstraint] {
        let topConstraint = self.containerScrollView.topAnchor.constraint(
            lessThanOrEqualTo: view.topAnchor,
            constant: scrollViewContainerTopConstant
        )
        topConstraint.priority = lowPriorityConstraintValue
        
        let bottomConstraint = self.containerScrollView.bottomAnchor.constraint(
            lessThanOrEqualTo: view.bottomAnchor,
            constant: scrollViewContainerBottomConstant
        )
        bottomConstraint.priority = lowPriorityConstraintValue
        
        let heightConstraint = self.containerScrollView.heightAnchor.constraint(greaterThanOrEqualToConstant: getScrollViewHeight())
        heightConstraint.priority = lowPriorityConstraintValue
                
        let containerViewHeight = CGFloat(numberOfRowsRequiredForQuiz) * ( textRepresentationViewHeightConstant + textRepresentationViewInterSpacing )

        return [
            topConstraint,
            bottomConstraint,
            heightConstraint,
            self.containerScrollView.heightAnchor.constraint(lessThanOrEqualToConstant: containerViewHeight)
        ]
    }
    
      private func getScrollViewHeight() -> CGFloat {
        if isOrientaionLandscape() {
            return UIScreen.main.bounds.height - scrollViewHeightDifference
        } else {
            return UIScreen.main.bounds.width - scrollViewHeightDifference
        }
      }
    
      private func getScrollViewWidth() -> CGFloat {
        if isOrientaionLandscape() {
            return UIScreen.main.bounds.height - scrollViewWidthDifference
        } else {
            return UIScreen.main.bounds.width - scrollViewWidthDifference
        }
      }
    
    private func setupQuizTextContainerView() -> [NSLayoutConstraint] {
        let quizTextContainerView = UIView()
        quizTextContainerView.translatesAutoresizingMaskIntoConstraints = false
        self.quizTextContainerView = quizTextContainerView
        containerScrollView.addSubview(self.quizTextContainerView)
                
        let containerViewHeight = CGFloat(numberOfRowsRequiredForQuiz) * ( textRepresentationViewHeightConstant + textRepresentationViewInterSpacing )

        return [
            self.quizTextContainerView.heightAnchor.constraint(equalToConstant: containerViewHeight),
            self.quizTextContainerView.leadingAnchor.constraint(equalTo: containerScrollView.leadingAnchor),
            self.quizTextContainerView.trailingAnchor.constraint(equalTo: containerScrollView.trailingAnchor),
            self.quizTextContainerView.topAnchor.constraint(equalTo: containerScrollView.topAnchor),
            self.quizTextContainerView.bottomAnchor.constraint(equalTo: containerScrollView.bottomAnchor),
            self.quizTextContainerView.widthAnchor.constraint(equalTo: containerScrollView.widthAnchor)
        ]
    }
    
    private func setupQuizTextRepresentationView() -> [NSLayoutConstraint] {
        var constraints : [NSLayoutConstraint] = []
        for index in 0 ..< numberOfRowsRequiredForQuiz {
            let currentQuizLine = quizLines[index]
            let quizLineWords = currentQuizLine.split(separator: " ")
            let randomBlankWordIndex = Int.random(in: Range(uncheckedBounds: (lower: 0, upper: quizLineWords.count)))
            let randomBlankWord = quizLineWords[randomBlankWordIndex]
            correctBlankWordsOrder[index] = String(describing: randomBlankWord)
            quizBlankWords.append(String(randomBlankWord))
            let textRepresentationView = QuizLineRepresentationView(
                with: quizLines[index],
                andBlankWordIndex: randomBlankWordIndex,
                representationViewOrder: index,
                delegate: self
            )
            textRepresentationView.translatesAutoresizingMaskIntoConstraints = false
            quizTextRepresentationViews.append(textRepresentationView)
            quizTextContainerView.addSubview(textRepresentationView)
            
            let currentQuixTextRepresentationTopContant = CGFloat(index) * (textRepresentationViewHeightConstant + textRepresentationViewInterSpacing)
            constraints += [
                textRepresentationView.topAnchor.constraint(equalTo: quizTextContainerView.topAnchor, constant: currentQuixTextRepresentationTopContant),
                textRepresentationView.leadingAnchor.constraint(equalTo: quizTextContainerView.leadingAnchor),
                textRepresentationView.trailingAnchor.constraint(equalTo: quizTextContainerView.trailingAnchor),
                textRepresentationView.heightAnchor.constraint(equalToConstant: textRepresentationViewHeightConstant),
            ]
        }
        return constraints
    }
    
    private func setupBlankWordsCollectionView() {
        let blankWordsCollectionViewFlowLayout = UICollectionViewFlowLayout()
        blankWordsCollectionViewFlowLayout.estimatedItemSize = blankWordsEstimatedItemSize
        blankWordsCollectionViewFlowLayout.minimumInteritemSpacing = blankSpacesInterItemSpacing
        self.blankWordsCollectionViewFlowLayout = blankWordsCollectionViewFlowLayout
        
        let blankWordsCollectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: self.blankWordsCollectionViewFlowLayout
        )
        blankWordsCollectionView.register(
            BlankWordsCollectionViewCell.self,
            forCellWithReuseIdentifier: blankWordsCollectionViewIdentifier
        )
        blankWordsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        blankWordsCollectionView.delegate = self
        blankWordsCollectionView.dataSource = self
        blankWordsCollectionView.showsHorizontalScrollIndicator = false
        blankWordsCollectionView.showsVerticalScrollIndicator = false
        blankWordsCollectionView.isHidden = true
        blankWordsCollectionView.backgroundColor = CurrentTheme.shared.blankWordsCollectionViewBackgroundColor
        self.blankWordsCollectionView = blankWordsCollectionView
        view.addSubview(self.blankWordsCollectionView)
        
        blankWordsCollectionViewPortraitConstraints = getCollectionViewPortraitConstraints()
        blankWordsCollectionViewLandsapeConstraints = getCollectionViewLandscapeConstraints()
        
        if isOrientaionLandscape() {
            blankWordsCollectionViewFlowLayout.scrollDirection = .vertical
            for constraint in blankWordsCollectionViewPortraitConstraints {
                constraint.isActive = false
            }
            for constraint in blankWordsCollectionViewLandsapeConstraints {
                constraint.isActive = true
            }
        } else {
            blankWordsCollectionViewFlowLayout.scrollDirection = .horizontal
            for constraint in blankWordsCollectionViewLandsapeConstraints {
                constraint.isActive = false
            }
            for constraint in blankWordsCollectionViewPortraitConstraints {
                constraint.isActive = true
            }
        }
    }
    
    private func getCollectionViewPortraitConstraints() -> [NSLayoutConstraint] {
        return [
            self.blankWordsCollectionView.topAnchor.constraint(equalTo: containerScrollView.bottomAnchor, constant: blankWordsCollectionViewBottomConstant),
            self.blankWordsCollectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            self.blankWordsCollectionView.heightAnchor.constraint(equalToConstant: blankWordsCollectionViewHeight),
            self.blankWordsCollectionView.widthAnchor.constraint(equalTo: containerScrollView.widthAnchor)
        ]
    }
    
    private func getCollectionViewLandscapeConstraints() -> [NSLayoutConstraint] {
        let leadingConstraint = self.blankWordsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: blankWordsCollectionViewLeadingConstant)
        leadingConstraint.priority = lowPriorityConstraintValue
        
        return [
            self.blankWordsCollectionView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            self.blankWordsCollectionView.heightAnchor.constraint(equalTo: containerScrollView.heightAnchor),
            self.blankWordsCollectionView.widthAnchor.constraint(lessThanOrEqualToConstant: blankWordsCollectionViewWidth),
            leadingConstraint,
            self.blankWordsCollectionView.trailingAnchor.constraint(equalTo: containerScrollView.leadingAnchor, constant: blankWordsCollectionViewTrailingConstant)
        ]
    }
    
    private func setupSubmitButton() {
        let submitButton = UIButton()
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.apply(theme: CurrentTheme.shared.quizSubmitButton)
        submitButton.setTitle("Check my score", for: .normal)
        submitButton.clipsToBounds = true
        submitButton.addTarget(self, action: #selector(onSubmitButtonTap), for: .touchUpInside)
        submitButton.layer.cornerRadius = quizSubmitButtonCornerRadius
        submitButton.layer.borderColor = UIColor.black.cgColor
        submitButton.layer.borderWidth = 0.5
        submitButton.titleLabel?.textAlignment = .center
        self.submitButton = submitButton
        view.addSubview(self.submitButton)
        
        submitButtonPortraitConstraints = getSubmitButtonPortraitConstraints()
        submitButtonLandscapeConstraints = getSubmitButtonLandscapeConstraints()
        
        if isOrientaionLandscape() {
            submitButton.titleLabel?.numberOfLines = 3
            for constraint in submitButtonPortraitConstraints {
                constraint.isActive = false
            }
            for constraint in submitButtonLandscapeConstraints {
                constraint.isActive = true
            }
        } else {
            submitButton.titleLabel?.numberOfLines = 1
            for constraint in submitButtonPortraitConstraints {
                constraint.isActive = true
            }
            for constraint in submitButtonLandscapeConstraints {
                constraint.isActive = false
            }
        }
    }
    
    private func getSubmitButtonPortraitConstraints() -> [NSLayoutConstraint] {
        return [
           submitButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
           submitButton.heightAnchor.constraint(equalToConstant: quizSubmitButtonHeightConstant),
           submitButton.widthAnchor.constraint(equalToConstant: quizSubmitButtonWidthConstant),
           submitButton.topAnchor.constraint(
               equalTo: containerScrollView.bottomAnchor,
               constant: blankWordsCollectionViewBottomConstant
           ),
       ]
    }
    
    private func getSubmitButtonLandscapeConstraints() -> [NSLayoutConstraint] {
        let trailingConstraint = submitButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: quizSubmitButtonTrailingConstant)
        trailingConstraint.priority = lowPriorityConstraintValue
        
        return [
           submitButton.leadingAnchor.constraint(equalTo: containerScrollView.trailingAnchor, constant: quizSubmitButtonLeadingConstant),
           trailingConstraint,
           submitButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
           submitButton.widthAnchor.constraint(lessThanOrEqualToConstant: quizSubmitButtonLandscapeSide),
           submitButton.heightAnchor.constraint(equalTo: submitButton.widthAnchor),
       ]
    }
    
    private func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
    
    private func setupScoreView() {
        let scoreView = ScoreView(
            withCorrectWords: correctBlankWordsOrder,
            answeredWords: answeredBlankWordsOrder,
            andDelegate: self
        )
        scoreView.translatesAutoresizingMaskIntoConstraints = false
        self.scoreView = scoreView
        view.addSubview(self.scoreView)
                
        NSLayoutConstraint.activate( [
            self.scoreView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            self.scoreView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            self.scoreView.widthAnchor.constraint(equalTo: containerScrollView.heightAnchor),
            self.scoreView.heightAnchor.constraint(equalTo: containerScrollView.widthAnchor)
        ])
    }
    
    private func setupActivityIndicatorView() {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.color = .white
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.startAnimating()
        self.activityIndicatorView = activityIndicatorView
        view.addSubview(self.activityIndicatorView)
        
        NSLayoutConstraint.activate ([
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicatorView.heightAnchor.constraint(equalToConstant: activityIndicatorHeight),
            activityIndicatorView.widthAnchor.constraint(equalToConstant: activityIndicatorWidth)
        ])
    }
    
    private func showBlankWordsCollectionView() {
        blankWordsCollectionView.isHidden = false
        if isOrientationPortrait() {
            submitButton.isHidden = true
        }
    }
    
    private func hideBlankWordsCollectionView() {
        blankWordsCollectionView.isHidden = true
        submitButton.isHidden = false
    }
    
    @objc private func onSubmitButtonTap() {
        addBlurEffect()
        showScore()
    }
    
    private func showScore() {
        setupScoreView()
    }
    
    private func isOrientaionLandscape() -> Bool {
        if UIScreen.main.bounds.height > UIScreen.main.bounds.width {
            return false
        }
        return true
    }
        
    private func isOrientationPortrait() -> Bool {
        if UIScreen.main.bounds.height > UIScreen.main.bounds.width {
            return true
        }
        return false
    }
}

// MARK: -

// MARK: ScoreViewDelegate

extension QuizViewController: ScoreViewDelegate {
    func onReplayButtonTap() {
        resetQuiz()
        setupActivityIndicatorView()
        delegate.replayQuiz(
            forFont: CurrentTheme.shared.quizLineRepresentationFont,
            width: getWidthForQuizText(),
            numberOfLines: numberOfRowsRequiredForQuiz,
            completionHandler: { [weak self] response in
                DispatchQueue.main.async {
                   self?.activityIndicatorView.stopAnimating()
                }
                switch response {
                case .success(let quizLines):
                    self?.quizLines = quizLines
                    self?.setupView()
                case .failure(let error):
                    print(error)
                    // handle error
                }
        })
    }
    
    private func resetQuiz() {
        removeSubviews()
        resetVariableValues()
    }
    
    private func removeSubviews() {
        for subview in view.subviews {
            subview.removeFromSuperview()
        }
    }
    
    private func resetVariableValues() {
        quizLines = []
        quizBlankWords = []
        correctBlankWordsOrder = [:]
        answeredBlankWordsOrder = [:]
        lastBlankWordIndexTapped = nil
        
        submitButtonPortraitConstraints = []
        submitButtonLandscapeConstraints = []
        
        blankWordsCollectionViewPortraitConstraints = []
        blankWordsCollectionViewLandsapeConstraints = []
        
        quizTextRepresentationViews = []
    }
}

// MARK: -

// MARK: QuizLineRepresentationViewDelegate

extension QuizViewController: QuizLineRepresentationViewDelegate {
    func onButtonTap(withTag tag: Int) {
        showBlankWordsCollectionView()
        lastBlankWordIndexTapped = tag
    }
}

// MARK: -

// MARK: UICollectionViewDelegate

extension QuizViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        quizTextRepresentationViews[lastBlankWordIndexTapped].updateQuizWordButtonTitle(to: quizBlankWords[indexPath.item])
        hideBlankWordsCollectionView()
        answeredBlankWordsOrder[lastBlankWordIndexTapped] = quizBlankWords[indexPath.item]
    }
}

// MARK: -

// MARK: UICollectionViewDataSource

extension QuizViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quizBlankWords.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: blankWordsCollectionViewIdentifier, for: indexPath) as! BlankWordsCollectionViewCell
        cell.setTitleLabelText(to: quizBlankWords[indexPath.item])
        cell.clipsToBounds = true
        cell.layer.cornerRadius = blankWordCollectionViewCellCornerRadius
        cell.layer.borderColor = CurrentTheme.shared.blankWordBorderColor.cgColor
        cell.layer.borderWidth = blankWordCollectionViewCellBorderWidth
        return cell
    }
}

// MARK: -

// MARK: Constants

private let numberOfRowsRequiredForQuiz = 10

private let blankWordCollectionViewCellCornerRadius: CGFloat = 5
private let blankWordCollectionViewCellBorderWidth: CGFloat = 0.5

private let scrollViewContainerTopConstant: CGFloat = 50
private let scrollViewContainerLeadingConstant: CGFloat = 10
private let scrollViewContainerTrailingConstant: CGFloat = -10
private let scrollViewContainerBottomConstant: CGFloat = -50
private let scrollViewContainerWidthConstant: CGFloat = 400

private let scrollViewHeightDifference: CGFloat = 60
private let scrollViewWidthDifference: CGFloat = 20

private let lowPriorityConstraintValue: UILayoutPriority = UILayoutPriority(rawValue: 999)

private let blankSpacesInterItemSpacing: CGFloat = 10
private let blankWordsEstimatedItemSize = CGSize(width: 100, height: 50)

private let blankWordsCollectionViewHeight: CGFloat = 70
private let blankWordsCollectionViewWidth: CGFloat = 120
private let blankWordsCollectionViewBottomConstant: CGFloat = 20
private let blankWordsCollectionViewLeadingConstant: CGFloat = 20
private let blankWordsCollectionViewTrailingConstant: CGFloat = -20

private let textRepresentationViewHeightConstant: CGFloat = 30
private let textRepresentationViewInterSpacing: CGFloat = 10

private let quizSubmitButtonWidthConstant: CGFloat = 200
private let quizSubmitButtonHeightConstant: CGFloat = 45
private let quizSubmitButtonCornerRadius: CGFloat = 5

private let quizSubmitButtonLandscapeSide: CGFloat = 100
private let quizSubmitButtonLeadingConstant: CGFloat = 20
private let quizSubmitButtonTrailingConstant: CGFloat = -20

private let quizLineWidthDifference: CGFloat = 40

private let activityIndicatorHeight: CGFloat = 50
private let activityIndicatorWidth: CGFloat = 50

private let scoreViewTopConstant: CGFloat = 30
private let scoreViewBottomConstant: CGFloat = -30
private let scoreViewLeadingConsatnt: CGFloat = 30
private let scoreViewTrailingConstant: CGFloat = -30
