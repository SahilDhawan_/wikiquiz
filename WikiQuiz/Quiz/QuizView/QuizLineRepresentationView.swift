//
//  QuizLineRepresentationView.swift
//  QuizGame
//
//  Created by Sahil Dhawan on 28/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Foundation
import UIKit

class QuizLineRepresentationView: UIView {
    private weak var preBlankWordLabel: UILabel!
    private weak var blankWordButton: UIButton!
    private weak var postBlankWordLabel: UILabel!
    
    private let representationText: String
    private let blankWordIndex: Int
    private let representationViewOrder: Int
    private weak var delegate: QuizLineRepresentationViewDelegate!
    
    init(
        with representationText: String,
        andBlankWordIndex blankWordIndex: Int,
        representationViewOrder: Int,
        delegate: QuizLineRepresentationViewDelegate
    ) {
        self.representationText = representationText
        self.blankWordIndex = blankWordIndex
        self.representationViewOrder = representationViewOrder
        self.delegate = delegate
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: -

// MARK: UI Setup
extension QuizLineRepresentationView {
    private func setupUI() {
        var constraints: [NSLayoutConstraint] = []
        constraints += setupPreBlankWordLabel()
        constraints += setupBlankWordButton()
        constraints += setupPostBlankWordLabel()
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupPreBlankWordLabel() -> [NSLayoutConstraint] {
        let preBlankWordLabel = UILabel()
        preBlankWordLabel.translatesAutoresizingMaskIntoConstraints = false
        preBlankWordLabel.text = getPreBlankWordText()
        preBlankWordLabel.apply(theme: CurrentTheme.shared.preBlankWordLabel)
        self.preBlankWordLabel = preBlankWordLabel
        
        preBlankWordLabel.sizeToFit()
        let labelSize = preBlankWordLabel.frame.size
        addSubview(self.preBlankWordLabel)
        
        return [
            self.preBlankWordLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            self.preBlankWordLabel.topAnchor.constraint(equalTo: topAnchor),
            self.preBlankWordLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            self.preBlankWordLabel.widthAnchor.constraint(equalToConstant: labelSize.width)
        ]
    }
    
    private func getPreBlankWordText() -> String {
        let textCharacters = representationText.split(separator: " ")
        var preBlankWordText: String = ""
        for index in 0 ..< blankWordIndex {
            preBlankWordText += textCharacters[index] + " "
        }
        return preBlankWordText
    }
    
    private func setupPostBlankWordLabel() -> [NSLayoutConstraint] {
        let postBlankWordLabel = UILabel()
        postBlankWordLabel.translatesAutoresizingMaskIntoConstraints = false
        postBlankWordLabel.apply(theme: CurrentTheme.shared.postBlankWordLabel)
        postBlankWordLabel.text = getPostBlankWordText()
        self.postBlankWordLabel = postBlankWordLabel
        
        postBlankWordLabel.sizeToFit()
        let labelSize = postBlankWordLabel.frame.size
        addSubview(self.postBlankWordLabel)
        
        return [
            self.postBlankWordLabel.leadingAnchor.constraint(equalTo: blankWordButton.trailingAnchor, constant: postBlankWordLabelLeadingConstant),
            self.postBlankWordLabel.topAnchor.constraint(equalTo: topAnchor),
            self.postBlankWordLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            self.postBlankWordLabel.widthAnchor.constraint(equalToConstant: labelSize.width),
            self.postBlankWordLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
    }
    
    private func getPostBlankWordText() -> String {
        let textCharacters = representationText.split(separator: " ")
        var postBlankWordText: String = ""
        for index in blankWordIndex + 1 ..< textCharacters.count {
            postBlankWordText += textCharacters[index] + " "
        }
        return postBlankWordText
    }
    
    private func setupBlankWordButton() -> [NSLayoutConstraint] {
        let quizWordButton = UIButton()
        quizWordButton.translatesAutoresizingMaskIntoConstraints = false
        quizWordButton.apply(theme: CurrentTheme.shared.blankWordButton)
        quizWordButton.tag = representationViewOrder
        quizWordButton.layer.borderColor = UIColor.darkGray.cgColor
        quizWordButton.layer.borderWidth = quizButtonBorderWidth
        quizWordButton.clipsToBounds = true
        quizWordButton.layer.cornerRadius = quizButtonBorderCornerRadius
        quizWordButton.addTarget(self, action: #selector(onQuizWordButtonTap), for: .touchUpInside)
        self.blankWordButton = quizWordButton
        addSubview(self.blankWordButton)
        
        return [
            quizWordButton.leadingAnchor.constraint(equalTo: preBlankWordLabel.trailingAnchor, constant: blankWordButtonLeadingConstant),
            quizWordButton.topAnchor.constraint(equalTo: topAnchor),
            quizWordButton.bottomAnchor.constraint(equalTo: bottomAnchor),
        ]
    }
    
    @objc private func onQuizWordButtonTap() {
        let buttonTag = blankWordButton.tag
        delegate.onButtonTap(withTag: buttonTag)
    }
    
    func updateQuizWordButtonTitle(to title: String) {
        blankWordButton.setTitle(title, for: .normal)
    }
}

// MARK: -

// MARK: QuizLineRepresentationViewDelegate

protocol QuizLineRepresentationViewDelegate: class {
    func onButtonTap(withTag tag: Int)
}


// MARK: -

// MARK: Constants

private let blankWordButtonLeadingConstant: CGFloat = 10
private let postBlankWordLabelLeadingConstant: CGFloat = 10
private let quizButtonBorderWidth: CGFloat = 0.5
private let quizButtonBorderCornerRadius: CGFloat = 5
