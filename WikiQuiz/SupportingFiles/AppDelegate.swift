//
//  AppDelegate.swift
//  QuizGame.swift
//
//  Created by Sahil Dhawan on 28/09/19.
//  Copyright © 2019 Sahil Dhawan. All rights reserved.
//

import Networking
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private var quizCoordinator: QuizCoordinator!
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let bounds = UIScreen.main.bounds
        window = UIWindow(frame: bounds)
        window!.makeKeyAndVisible()
        quizCoordinator = QuizCoordinator()
        let rootVC = quizCoordinator.presentRootViewController()
        window!.rootViewController = rootVC
        return true
    }
}

